#ifndef ALIASES_INT_H
#define ALIASES_INT_H

#include <cstdint>


using ubyte = unsigned char;
using int32 = std::int32_t;
using uint32 = std::uint32_t;

#endif // ALIASES_INT_H
