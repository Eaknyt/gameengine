#ifndef GRID_H
#define GRID_H

#include "render/geometry/geometry.h"


Geometry grid(int size, float height = 0.);

#endif // GRID_H
