#ifndef HEIGHTMAP_H
#define HEIGHTMAP_H

#include "render/geometry/geometry.h"


Geometry heightmapToGeometry(const QImage &heightmap);

#endif // HEIGHTMAP_H
