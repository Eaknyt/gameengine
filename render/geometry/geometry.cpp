#include "geometry.h"

const int Geometry::vertexSize = sizeof(Vertex);
const int Geometry::indexSize = sizeof(uint32);
