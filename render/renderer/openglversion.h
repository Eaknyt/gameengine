#ifndef OPENGLVERSION_H
#define OPENGLVERSION_H

#include <QOpenGLFunctions_3_3_Core>


using OpenGLFuncs = QOpenGLFunctions_3_3_Core;

#endif // OPENGLVERSION_H
