#ifndef RENDER_MATERIALS_BUILTINS_H
#define RENDER_MATERIALS_BUILTINS_H

#include "render/material/material.h"


Material defaultMaterial();
Material phongMaterial();
Material phongDirectionalMaterial();

#endif // RENDER_MATERIALS_BUILTINS_H
